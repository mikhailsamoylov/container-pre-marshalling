import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Chromosome extends Matrix<Integer> implements Iterable<Gene> {
	
	private Bay initialStateBay;
	private Bay finalStateBay;
	private String solutionAsString;
	
	public Chromosome() {
		super();
		solutionAsString = getSolutionAsString();
	}
	
	@SuppressWarnings("unchecked")
	public Chromosome clone() {
		Chromosome clonedChromosome = new Chromosome();
        
		clonedChromosome.matrix = (HashMap<Coordinate, Integer>) matrix.clone();
		clonedChromosome.maxColumns = (HashMap<Integer, Integer>) maxColumns.clone();
		clonedChromosome.minColumns = (HashMap<Integer, Integer>) minColumns.clone();
		clonedChromosome.maxRow = maxRow;
		clonedChromosome.minRow = minRow;
		if (initialStateBay != null) clonedChromosome.initialStateBay = initialStateBay.clone();
		if (finalStateBay != null) clonedChromosome.finalStateBay = finalStateBay.clone();
		clonedChromosome.solutionAsString = solutionAsString;
        
        return clonedChromosome;
    }
	
	public Bay getInitialStateBay() {
		return initialStateBay;
	}
	
	public void setInitialStateBay(Bay initialStateBay) {
		this.initialStateBay = initialStateBay.clone();
	}
	
	public Bay getFinalStateBay() {
		if (finalStateBay == null || !solutionAsString.equals(getSolutionAsString())) {
			Bay newBay = initialStateBay.clone();
			Iterator<Gene> iterator = iterator();
			while(iterator.hasNext()) {
				Gene gene = iterator.next();
				BayManager.moveContainer(newBay, gene.getFrom(), gene.getTo());
			}
			setFinalStateBay(newBay);
		}
		
		return finalStateBay;
	}
	
	public String getSolutionAsString() {
		String sol = "";
		Iterator<Gene> iterator = iterator();
		while (iterator.hasNext()) {
			Gene gene = iterator.next();
			sol += Integer.toString(gene.getFrom()) + Integer.toString(gene.getTo()) + "|";
		}
		
		return sol;
	}

	public void setFinalStateBay(Bay finalStateBay) {
		this.finalStateBay = finalStateBay.clone();
		solutionAsString = getSolutionAsString();
	}
	
	public void deleteGene(int rowIndex) {
		int geneCount = getMaxRowIndex() + 1;
		
		deleteCoordinate(new Coordinate(rowIndex, 0));
		deleteCoordinate(new Coordinate(rowIndex, 1));
		
		for (int i = rowIndex; i < geneCount - 1; i++) {
			setCoordinate(i, 0, getValueByCoordinate(i + 1, 0));
			setCoordinate(i, 1, getValueByCoordinate(i + 1, 1));
		}
		
		deleteCoordinate(new Coordinate(geneCount - 1, 0));
		deleteCoordinate(new Coordinate(geneCount - 1, 1));
		
		maxRow--;
	}
	
	public void setGene(int rowIndex, int from, int to) {
		setCoordinate(rowIndex, 0, from);
		setCoordinate(rowIndex, 1, to);
	}
	
	public void setGene(int rowIndex, Gene gene) {
		setGene(rowIndex, gene.getFrom(), gene.getTo());
	}
	
	public Gene getGene(int rowIndex) {
		return new Gene(getValueByCoordinate(rowIndex, 0), getValueByCoordinate(rowIndex, 1));
	}
	
	public void printMe() {
		for (int i = getMinRowIndex(); i <= getMaxRowIndex(); i++) {
			try {
				System.out.print(getValueByCoordinate(i,  0) + " " + getValueByCoordinate(i, 1));
			} catch (Exception e) {
				e.getMessage();
			}
			if (i < getMaxRowIndex()) System.out.print(" | ");
		}
		System.out.println();
	}

	
	
	public class ChromosomeIterator implements Iterator<Gene> {

		private int rowPointer;
		
		@Override
		public boolean hasNext() {
			return maxRow >= rowPointer;
		}

		@Override
		public Gene next() {
			if (hasNext()) {
				Gene gene = new Gene(getValueByCoordinate(rowPointer, 0), getValueByCoordinate(rowPointer, 1));
				rowPointer++;
				return gene;
			} else {
				throw new NoSuchElementException();
			}
		}
		
	}

	@Override
	public Iterator<Gene> iterator() {
		return new ChromosomeIterator();
	}
	
	public int size() {
		return maxRow + 1;
	}
	
	public boolean isFeasible() {
		return isFeasible(true);
	}

	public boolean isFeasible(boolean checkSize) {
		if (checkSize && size() < BayGenetics.getLowerBound()) {
			return false;
		}
		
		Bay newBay = initialStateBay.clone();
		
		Iterator<Gene> iterator = iterator();
		while (iterator.hasNext()) {
			Gene gene = iterator.next();
			int from = gene.getFrom();
			int to = gene.getTo();
			if (newBay.getMaxColumnIndex(from) == -1) {
				return false;
			}
			if (newBay.getMaxColumnIndex(to) >= initialStateBay.getMaxTierIndex()) {
				return false;
			}
			
			BayManager.moveContainer(newBay, from, to);
		}
		setFinalStateBay(newBay);
		
		return true;
	}
	
	public Chromosome append(Chromosome chromosomeToAppend) {
		if (chromosomeToAppend != null) {
			int firstFreeIndex = size();
			int chromosomeToAppendSize = chromosomeToAppend.size();
			int to = firstFreeIndex + chromosomeToAppendSize;
			
			for (int i = firstFreeIndex, j = 0; i < to; i++, j++) {
				this.setGene(i, chromosomeToAppend.getGene(j));
			}
			
			return this;
		}
		
		return null;
	}
}
