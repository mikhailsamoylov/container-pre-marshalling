import java.io.File;
import java.util.List;

public class Main {	
	public static void main(String[] args) {
		Logger.setEnabled(true);
		
//		String instanceDir = "test_instances/";
		String outputDir = "output/";
//		//String filename = instanceDir + "data3-3-6.dat";
//		String filename = instanceDir + "test.dat";
//		IParser parser = new Parser(filename);
//		Bay bay = parser.getBay();
//		bay.setMaxTierIndex(25); //lee_and_chao.dat
////		b.setMaxTierIndex(3); //lee_and_hsu.dat
////		Logger.logging("Initial layout:");
////		b.printMe();
////		BayGenetics.setMutationRule(MutationRuleWithStep.class, .0);
//		long timeStart = System.currentTimeMillis();
//		Chromosome solution = BayGenetics.calculate(bay);
//		if (solution == null) {
//			Logger.logging("No solution found");
//		} else {
//			Logger.logging("The solution is: ", false);
//			solution.printMe();
//			Logger.logging("Working time: " + ((System.currentTimeMillis() - timeStart) / 1000) + "s, Movements: " + solution.size());
//		}
		
		
//		Map<String, Integer> instanceToMaxTier = new HashMap<>();
//		instanceToMaxTier.put("lee_and_hsu.dat", 3);
//		instanceToMaxTier.put("lee_and_chao.dat", 4);
//		BayGenetics.setMutationRule(MutationRuleWithStep.class, .0);
//		
//		for (String instance : new String[]{"lee_and_chao.dat"}) {
//			String filename = instanceDir + instance;
//			Parser parser = new Parser();
//			Bay b = parser.parse(filename);
//			b.setMaxTierIndex(instanceToMaxTier.get(instance));
//			for (int popSize : new int[]{100, 300}) {
//				BayGenetics.maxNumberOfGenerations = popSize;
//				for (double crossoverProb : new double[]{0.1, 0.3, 0.5, 0.7, 0.9}) {
//					BayGenetics.crossoverProbability = crossoverProb;
//					for (double mutationProb : new double[]{0.1, 0.3, 0.5, 0.7, 0.9}) {
//						BayGenetics.growthMutationProbability = mutationProb;
//						BayGenetics.shrinkMutationProbability = mutationProb;
//						BayGenetics.swapMutationProbability = mutationProb;
//						BayGenetics.replaceMutationProbability = mutationProb;
//						for (int i = 0; i < 10; i++) {
//							long timeStart = System.currentTimeMillis();
//							Chromosome solution = BayGenetics.calculate(b);
//							long timeEnd = System.currentTimeMillis();
//							Solution sol = new Solution(outputDir + instance, solution, timeEnd - timeStart, popSize, crossoverProb, mutationProb);
//							sol.save();
//						}
//					}
//				}
//			}
//		}
		
			String instanceDir = "test_instances/cpmp_testcases/";
			for (String directory : new String[]{"LC2b/", "LC3a/", "LC3b/"}) {
				for (String filename : FileSystemHelper.listFilesForFolder(new File(instanceDir + directory))) {
					switch (filename) {
						case "lc2b_1.bay":
						case "lc2b_10.bay":
						case "lc2b_2.bay":
						case "lc2b_3.bay":
						case "lc2b_4.bay":
						case "lc2b_5.bay":
							continue;
					}
					IParser parser = new Parser2(instanceDir + directory + filename);
					Bay bay = parser.getBay();
					bay.setMaxTierIndex(parser.getHeight() - 1);
					bay.printMe();
					
					for (int popSize : new int[]{50}) {
						BayGenetics.maxNumberOfGenerations = popSize;
						for (double crossoverProb : new double[]{0.3}) {
							BayGenetics.crossoverProbability = crossoverProb;
							for (double mutationProb : new double[]{0.5}) {
								BayGenetics.growthMutationProbability = mutationProb;
								BayGenetics.shrinkMutationProbability = mutationProb;
								BayGenetics.swapMutationProbability = mutationProb;
								BayGenetics.replaceMutationProbability = mutationProb;
								for (int i = 0; i < 10; i++) {
									long timeStart = System.currentTimeMillis();
									Chromosome solution = BayGenetics.calculate(bay, 0);
									long timeEnd = System.currentTimeMillis();
									Solution sol = new Solution(outputDir + filename, solution, timeEnd - timeStart, popSize, crossoverProb, mutationProb);
									sol.save();
								}
							}
						}
					}
				}
			}
		
	}
}
