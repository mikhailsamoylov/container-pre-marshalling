import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class BayGenetics {
	
	public static  int populationSize = 10000;
	public static  int maxNumberOfGenerations = 100;
	public static  double crossoverProbability = 0.1;
	public static  double growthMutationProbability = 0.3;
	public static  double shrinkMutationProbability = 0.3;
	public static  double swapMutationProbability = 0.3;
	public static  double replaceMutationProbability = 0.3;
	
	public static final double childrenSelectionRate = 0.9;
	public static final double parentsSelectionRate = 0.02;
	public static final double randomSelectionRate = 0.08;
	public static final int iterationsCount = 10000;
	public static final int levelsLimit = 10;
	
	private static int lowerBound;
	
	public static void setLowerBound(int lowerBound) {
		BayGenetics.lowerBound = lowerBound;
	}
	
	public static int getLowerBound() {
		return lowerBound;
	}
	
	public static Chromosome calculate(Bay bay, int level) {	
		int bortfeldtLB = calculateLowerBound(bay);
		Matrix<Integer> possibleMovements = getPossibleMovements(bay);
		ArrayList<Chromosome> chromosomes = generateParents(possibleMovements, bortfeldtLB, bay);
		chromosomes = movementsElimination(chromosomes);
		chromosomes = eliteSelection(chromosomes, bay, maxNumberOfGenerations);
		Logger.logging("OK");
		System.out.println();
		
		setLowerBound(bortfeldtLB);
		Chromosome bestChromosome = null;
		for (int i = 0; i < iterationsCount; i++) {
			Logger.logging("level #" + level + ", iteration #" + i);
			
			ArrayList<Chromosome> chromosomesAfterCrossover = crossover(chromosomes);			
			ArrayList<Chromosome> chromosomesAfterMutation = mutations(chromosomesAfterCrossover, possibleMovements, i);
			ArrayList<Chromosome> chromosomesAfterElimination = movementsElimination(chromosomesAfterMutation);
			ArrayList<Chromosome> feasibleChromosomes = getFeasibleSolutions(chromosomesAfterElimination, bay);
			ArrayList<Chromosome> chromosomesAfterRWSelection = rouletteWheelSelection(feasibleChromosomes, bay, (int)(maxNumberOfGenerations * childrenSelectionRate));
			ArrayList<Chromosome> chromosomesAfterEliteSelection = eliteSelection(chromosomes, bay, (int)(maxNumberOfGenerations * parentsSelectionRate));
			ArrayList<Chromosome> generatedChromosomes = generateFeasibleChromosomes(possibleMovements, getLowerBound(), bay, (int)(maxNumberOfGenerations * randomSelectionRate));
			chromosomes = mergeAndSort(chromosomesAfterRWSelection, chromosomesAfterEliteSelection, generatedChromosomes);
			
			if (chromosomes.size() == 0) {
				Logger.logging("There is no chromosomes here:(");
				return null;
			}
			
			bestChromosome = chromosomes.get(0);
			
			if (i == iterationsCount) {
				bestChromosome = doSwapLocalSearch(bestChromosome);
				bestChromosome = doReplaceLocalSearch(bestChromosome);
				chromosomes.set(0, bestChromosome);
			}
			
			Bay bestChromosomeBay = bestChromosome.getFinalStateBay();
			int currentMI = bestChromosomeBay.getMisOverlayIndex();
			
			Logger.logging("\nMis-overlay index: " + currentMI);
			
			Logger.logging("Final layout:");
			bestChromosomeBay.printMe();
			
			if (currentMI == 0) {
				return bestChromosome;
			} else {
				Logger.logging("Chromosome: ", false);
				bestChromosome.printMe();
			}
			
			System.out.println();
		}
		
		if (level < levelsLimit) {
			return bestChromosome.append(calculate(bestChromosome.getFinalStateBay(), level + 1));
		} else {
			return null;
		}
	}

	private static int calculateLowerBound(Bay bay) {
		return LowerBound.calculate(bay);
	}

	private static ArrayList<Chromosome> mutations(
			ArrayList<Chromosome> chromosomes,
			Matrix<Integer> possibleMovements,
			int iteration
	) {
		Logger.logging("mutations... ", false);
		
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();
		for (Chromosome chromosome : chromosomes) {
			Chromosome newChromosome = chromosome.clone();
			if (Math.random() < growthMutationProbability) {
				int geneCount = chromosome.getMaxRowIndex() + 1;
				int geneIndex = (int) (Math.random() * (geneCount + 1));
				int possibleMovementIndex = (int) (Math.random() * possibleMovements.getMaxRowIndex() + 1);
				
				newChromosome.setCoordinate(geneIndex, 0, possibleMovements.getValueByCoordinate(possibleMovementIndex, 0));
				newChromosome.setCoordinate(geneIndex, 1, possibleMovements.getValueByCoordinate(possibleMovementIndex, 1));
				
				for (int i = geneIndex; i < geneCount; i++) {
					newChromosome.setCoordinate(i + 1, 0, chromosome.getValueByCoordinate(i, 0));
					newChromosome.setCoordinate(i + 1, 1, chromosome.getValueByCoordinate(i, 1));
				}
				
				chromosome = newChromosome.clone();
			}
			if (Math.random() < shrinkMutationProbability) {
				int geneCount = chromosome.getMaxRowIndex() + 1;
				if (geneCount > 0) {
					chromosome.deleteGene((int) (Math.random() * geneCount));
					chromosome = newChromosome.clone();
				}
			}
			if (Math.random() < swapMutationProbability) {
				int geneCount = chromosome.getMaxRowIndex() + 1;
				int gene1Index = (int) (Math.random() * geneCount);
				int gene2Index = (int) (Math.random() * geneCount);
				
				newChromosome.setCoordinate(gene1Index, 0, chromosome.getValueByCoordinate(gene2Index, 0));
				newChromosome.setCoordinate(gene1Index, 1, chromosome.getValueByCoordinate(gene2Index, 1));
				
				newChromosome.setCoordinate(gene2Index, 0, chromosome.getValueByCoordinate(gene1Index, 0));
				newChromosome.setCoordinate(gene2Index, 1, chromosome.getValueByCoordinate(gene1Index, 1));
				
				chromosome = newChromosome.clone();
			}
			if (Math.random() < replaceMutationProbability) {
				int geneCount = chromosome.getMaxRowIndex() + 1;
				int geneIndex = (int) (Math.random() * geneCount);
				int possibleMovementIndex = (int) (Math.random() * possibleMovements.getMaxRowIndex() + 1);
				
				newChromosome.setCoordinate(geneIndex, 0, possibleMovements.getValueByCoordinate(possibleMovementIndex, 0));
				newChromosome.setCoordinate(geneIndex, 1, possibleMovements.getValueByCoordinate(possibleMovementIndex, 1));
				
				chromosome = newChromosome.clone();
			}
			
			newChromosomes.add(newChromosome);
		}
		
		return newChromosomes;
	}

	private static ArrayList<Chromosome> crossover(ArrayList<Chromosome> chromosomes) {
		Logger.logging("crossover... ", false);
		
		ArrayList<Chromosome> allChildren = new ArrayList<>();
		
		for (Chromosome i : chromosomes) {
			for (Chromosome j : chromosomes) {
				if (i == j) continue;
				
				if (Math.random() < crossoverProbability) {
					allChildren.addAll(doCrossover(i, j));
				}
			}
		}
		
		Logger.logging(allChildren.size() + " | ", false);
		return allChildren;
	}

	private static ArrayList<Chromosome> doCrossover(Chromosome i, Chromosome j) {
		int iSize = i.getMaxRowIndex();
		int jSize = j.getMaxRowIndex();
		double randNum = Math.random() * Math.min(iSize, jSize);
		Chromosome firstChild = new Chromosome();
		Chromosome secondChild = new Chromosome();
		
		for (int k = 0; k <= jSize; k++) {
			Chromosome l = k < randNum ? i : j;
			firstChild.setCoordinate(k, 0, l.getValueByCoordinate(k, 0));
			firstChild.setCoordinate(k, 1, l.getValueByCoordinate(k, 1));
		}
		
		for (int k = 0; k <= iSize; k++) {
			Chromosome l = k < randNum ? j : i;
			secondChild.setCoordinate(k, 0, l.getValueByCoordinate(k, 0));
			secondChild.setCoordinate(k, 1, l.getValueByCoordinate(k, 1));
		}
		
		ArrayList<Chromosome> children = new ArrayList<>();
		children.add(firstChild);
		children.add(secondChild);
		
		return children;
	}

	private static Matrix<Integer> getPossibleMovements(Bay bay) {
		int j = bay.getMaxRowIndex() + 1;
		Matrix<Integer> possibleMovements = new Matrix<>();
		int l = 0;
		for (int i = 0; i < j; i++) {
			for (int k = 0; k < j; k++) {
				if (i != k) {
					possibleMovements.setCoordinate(l, 0, i);
					possibleMovements.setCoordinate(l, 1, k);
					l++;
				}
			}
		}
		
		return possibleMovements;
	}
	
	private static ArrayList<Chromosome> generateParents(Matrix<Integer> possibleMovements, int lowerBound, Bay bay) {
		Logger.logging("parents generation... ", false);
		return generateFeasibleChromosomes(possibleMovements, lowerBound, bay, populationSize);
	}
	
	private static ArrayList<Chromosome> generateFeasibleChromosomes(Matrix<Integer> possibleMovements, int lowerBound, Bay bay, int count) {
		ArrayList<Chromosome> chromosomes = new ArrayList<>();
		
		for (int i = 0; i < count; i++) {
			int rnc = (int)(Math.random() * lowerBound) + lowerBound;
			chromosomes.add(generateFeasibleChromosome(rnc, bay, possibleMovements));
		}
		
		return chromosomes;
	}
	
	private static Chromosome generateFeasibleChromosome(int length, Bay bay, Matrix<Integer> possibleMovements) {
		Chromosome cr = new Chromosome();
		cr.setInitialStateBay(bay);
		for (int m = 0; m < length; m++) {
			int geneIndex = (int)(Math.random() * (possibleMovements.getMaxRowIndex() + 1));
			cr.setGene(m, new Gene(
					possibleMovements.getValueByCoordinate(geneIndex, 0),
					possibleMovements.getValueByCoordinate(geneIndex, 1)
			));
			if (!cr.isFeasible(false)) {
				cr.deleteGene(m);
				m--;
			}
		}
		
		return cr;
	}

	private static ArrayList<Chromosome> movementsElimination(ArrayList<Chromosome> chromosomes) {
		Logger.logging("movements elimination... ", false);
		
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();
		for (Chromosome chromosome : chromosomes) {
			Chromosome newChromosome = chromosome.clone();
			int k = 0;
			for (int i = 0; i < newChromosome.getMaxRowIndex(); i++) {
				int j = i + 1;
				while (j < newChromosome.getMaxRowIndex() + 1) {
					int currentSource = newChromosome.getValueByCoordinate(i, 0);
					int currentDest = newChromosome.getValueByCoordinate(i, 1);
					int nextSource = newChromosome.getValueByCoordinate(i + 1, 0);
					int nextDest =  newChromosome.getValueByCoordinate(i + 1, 1);
					
					if (currentDest == nextSource) {
						if (currentSource == nextDest) {
							newChromosome.setGene(k, currentSource, currentDest);
						} else {
							newChromosome.setGene(k, currentSource, nextDest);
						}
						newChromosome.deleteGene(k + 1);
					} else {
						newChromosome.setGene(k, currentSource, currentDest);
						k++;
						break;
					}
				}
			}
			newChromosomes.add(newChromosome);
		}
		
		return newChromosomes;
	}
	
	private static ArrayList<Chromosome> getFeasibleSolutions(ArrayList<Chromosome> chromosomes, Bay bay) {
		Logger.logging("feasible solutions search... ", false);
		
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();
		for (Chromosome chromosome : chromosomes) {
			chromosome.setInitialStateBay(bay);
			if (chromosome.isFeasible()) {
				newChromosomes.add(chromosome);
			}
		}
		
		Logger.logging(newChromosomes.size() + " | ", false);
		return newChromosomes;
	}
	
	private static ArrayList<Chromosome> rouletteWheelSelection(ArrayList<Chromosome> chromosomes, Bay bay, int numberOfChromosomes) {
		Logger.logging("RW selection... ", false);
		
		ArrayList<Chromosome> shuffledChromosomes = new ArrayList<>(chromosomes);
		Collections.shuffle(shuffledChromosomes);
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();

		if (shuffledChromosomes.size() > numberOfChromosomes) {
			double partSize = shuffledChromosomes.size() / (double)numberOfChromosomes; 
			for (int i = 0; i < numberOfChromosomes; i++) {
				int leftBound = (int)(i * partSize);
				int rightBound = (int)((i + 1) * partSize);
				ArrayList<Chromosome> part = new ArrayList<Chromosome>(shuffledChromosomes.subList(leftBound, rightBound));
				
				int maxMI = Integer.MIN_VALUE;
				int maxL = Integer.MIN_VALUE;
				for (Chromosome chromosome : part) {
					Bay newBay = bay.clone();
					int genesCount = chromosome.getMaxRowIndex() + 1;
					for (int j = 0; j < genesCount; j++) {
						int from = chromosome.getValueByCoordinate(j, 0);
						int to = chromosome.getValueByCoordinate(j, 1);
						BayManager.moveContainer(newBay, from, to);
					}
					chromosome.setFinalStateBay(newBay);
					
					// find max mis-overlay index
					int currentMI = chromosome.getFinalStateBay().getMisOverlayIndex();
					if (currentMI > maxMI) {
						maxMI = currentMI;
					}
					
					// find max length of chromosome
					int currentLength = chromosome.getElementNumbers();
					if (currentLength > maxL) {
						maxL = currentLength;
					}
				}
				
				// calculate LiHead
				int[] liHeads = new int[part.size()];
				int sumOfLiHead = 0;
				int j = 0;
				for (Chromosome chromosome : part) {
					liHeads[j] = maxL - chromosome.getElementNumbers() + 1;
					sumOfLiHead += liHeads[j];
					j++;
				}
				
				// calculate rating
				double[] ratings = new double[part.size()];
				double ratingSum = 0;
				j = 0;
				for (Chromosome chromosome : part) {
					ratings[j] = maxMI - chromosome.getFinalStateBay().getMisOverlayIndex() + liHeads[j] / (double)sumOfLiHead;
					ratingSum += ratings[j];
					j++;
				}
				
				// build a distribution function
				double[] distribution = new double[part.size() + 1];
				distribution[0] = 0;
				for (j = 0; j < part.size(); j++) {
					double normalizedRating = ratings[j] / ratingSum;
					distribution[j + 1] = distribution[j] + normalizedRating;
				}
				
				double randNum = Math.random();
				for (j = 1; j < distribution.length; j++) {
					if (distribution[j - 1] < randNum && distribution[j] >= randNum) {
						newChromosomes.add(part.get(j - 1));
						break;
					}
				}
			}
		} else {
			newChromosomes = new ArrayList<>(chromosomes);
			for (Chromosome chromosome : newChromosomes) {
				chromosome.setInitialStateBay(bay);
				Bay newBay = bay.clone();
				Iterator<Gene> iterator = chromosome.iterator();
				while(iterator.hasNext()) {
					Gene gene = iterator.next();
					BayManager.moveContainer(newBay, gene.getFrom(), gene.getTo());
				}
				chromosome.setFinalStateBay(newBay);
			}
		}
		
		Logger.logging(newChromosomes.size() + " | ", false);
		return newChromosomes;
	}

	private static ArrayList<Chromosome> eliteSelection(ArrayList<Chromosome> chromosomes, Bay bay, int numberOfChromosomes) {
		Logger.logging("elite selection... ", false);
		ArrayList<Chromosome> newChromosomes = new ArrayList<>(chromosomes.subList(0, Math.min(numberOfChromosomes, chromosomes.size())));
		Logger.logging(newChromosomes.size() + " | ", false);
		return newChromosomes;
	}
	
	@SafeVarargs
	private static ArrayList<Chromosome> mergeAndSort(ArrayList<Chromosome>... chromosomes) {
		Logger.logging("chromosome merging... ", false);
		
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();
		for (ArrayList<Chromosome> chr : chromosomes) {
			newChromosomes.addAll(chr);
		}
		
		Collections.sort(newChromosomes, new Comparator<Chromosome>() {
			@Override
			public int compare(Chromosome chr1, Chromosome chr2) {
				int chr1MI = chr1.getFinalStateBay().getMisOverlayIndex();
				int chr2MI = chr2.getFinalStateBay().getMisOverlayIndex();
				if (chr1MI < chr2MI) {
					return -1;
				} else if (chr1MI == chr2MI) {
					int chr1EN = chr1.getElementNumbers();
					int chr2EN = chr2.getElementNumbers();
					return chr1EN < chr2EN ? -1 : chr1EN > chr2EN ? 1 : 0;
				} else {
					return 1;
				}
			}
		});
		
		Logger.logging(newChromosomes.size() + " | ", false);
		return newChromosomes;
	}
	
	@SuppressWarnings("unused")
	private static ArrayList<Chromosome> localSearch(ArrayList<Chromosome> chromosomes) {
		Logger.logging("local search... ", false);
		ArrayList<Chromosome> newChromosomes = new ArrayList<>();
		
		for (Chromosome chromosome : chromosomes) {
			double rand = Math.random();
			if (rand < 0.5) {
				newChromosomes.add(doSwapLocalSearch(chromosome));
			} else {
				newChromosomes.add(doReplaceLocalSearch(chromosome));
			}
		}
		
		return newChromosomes;
	}
	
	private static Chromosome doReplaceLocalSearch(Chromosome chromosome) {
		int chromosomeSize = chromosome.size();
		Chromosome currentBest = chromosome.clone();
		Matrix<Integer> allPossibleMovements = getPossibleMovements(chromosome.getInitialStateBay());
		int allPossibleMovementsSize = allPossibleMovements.getMaxRowIndex() + 1;
		for (int i = 0; i < chromosomeSize; i++) {
			for (int j = 0; j < allPossibleMovementsSize; j++) {
				Gene newGene = new Gene(allPossibleMovements.getValueByCoordinate(j, 0), allPossibleMovements.getValueByCoordinate(j, 1));
				Gene oldGene = chromosome.getGene(i);
				if (!newGene.equals(oldGene)) {
					Chromosome current = currentBest.clone();
					current.setGene(i, newGene);
					
					if (current.isFeasible() && current.getFinalStateBay().getMisOverlayIndex() < currentBest.getFinalStateBay().getMisOverlayIndex()) {
						currentBest = current.clone();
						i = -1;
						break;
					}
				}
			}
		}
		
		return currentBest;
	}

	private static Chromosome doSwapLocalSearch(Chromosome chromosome) {
		int chromosomeSize = chromosome.size();
		Chromosome currentBest = chromosome.clone();
		for (int i = 0; i < chromosomeSize - 1; i++) {
			for (int j = i + 1; j < chromosomeSize; j++) {
				Chromosome current = currentBest.clone();
				Gene gene1 = chromosome.getGene(j);
				Gene gene2 = chromosome.getGene(i);
				current.setGene(i, gene1);
				current.setGene(j, gene2);
				
				if (current.isFeasible() && current.getFinalStateBay().getMisOverlayIndex() < currentBest.getFinalStateBay().getMisOverlayIndex()) {
					currentBest = current.clone();
					i = -1;
					break;
				}
			}
		}
		
		return currentBest;
	}
}
