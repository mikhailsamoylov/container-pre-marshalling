import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class LowerBound {
	public static int calculate(Bay bay) {
		int lowerBoundOfBX = getLowerBoundOfBX(bay);
		int lowerBoundOfGX = getLowerBoundOfGX(bay);
		return lowerBoundOfBX + lowerBoundOfGX;
	}

	private static int getLowerBoundOfGX(Bay bay) {
		int gStar = getGStar(bay);
		Set<Integer> potentialGXStackIndices = getPotentialGXStackIndices(bay, gStar);
		int numberOfGX = getNumberOfGX(bay, gStar);
		Map<Integer, Integer> newGXStacksOrder = getNewGXStacksMapWithOrdering(bay, potentialGXStackIndices);
		
		return calculateLowerBoundOfGX(numberOfGX, newGXStacksOrder);
	}

	private static int calculateLowerBoundOfGX(int numberOfGX, Map<Integer, Integer> newGXStacksOrder) {
		int lowerBoundOfGX = 0;
		int i = 0;
		
		for (Map.Entry<Integer, Integer> entry : newGXStacksOrder.entrySet()) {
			if (i >= numberOfGX) {
				break;
			}
			lowerBoundOfGX += entry.getValue();
			i++;
		}
		
		return lowerBoundOfGX;
	}

	private static Map<Integer, Integer> getNewGXStacksMapWithOrdering(Bay bay, Set<Integer> potentialGXStackIndices) {
		Map<Integer, Integer> stackIndexToMaxTier = new LinkedHashMap<>();
		
		for (int stackIndex : potentialGXStackIndices) {
			int[] highestWellPlacedItemForStack = getHighestWellPlacedItem(bay, stackIndex);
			int highestWellPlacedTier = highestWellPlacedItemForStack[1];
	
			stackIndexToMaxTier.put(stackIndex, highestWellPlacedTier + 1);
		}
		
		return MapUtil.sortByValue(stackIndexToMaxTier);
	}

	private static int getNumberOfGX(Bay bay, int gStar) {
		Map<Integer, Integer> cumulativeDemandSurplus = getCumulativeDemandSurplus(bay);
		int cumulativeDemandSurplusGStar = cumulativeDemandSurplus.get(gStar);
		
		return (int) (Math.max(0, Math.ceil((double) cumulativeDemandSurplusGStar / bay.getMaxTierIndex())));
	}

	private static Set<Integer> getPotentialGXStackIndices(Bay bay, int gStar) {
		Set<Integer> potentialGXStackIndices = new TreeSet<>();
		
		int k = bay.getMaxRowIndex() + 1;
		for (int stackIndex = 0; stackIndex < k; stackIndex++) {
			int[] highestWellPlacedItemForStack = getHighestWellPlacedItem(bay, stackIndex);
			int highestWellPlacedItemGroup = highestWellPlacedItemForStack[0];
			if (highestWellPlacedItemGroup < gStar) {
				potentialGXStackIndices.add(stackIndex);
			}
		}
		
		return potentialGXStackIndices;
	}

	private static int getGStar(Bay bay) {
		Map<Integer, Integer> cumulativeDemandSurplus = getCumulativeDemandSurplus(bay);
		int maxCumulativeDemandSurplus = Integer.MIN_VALUE;
		int currentGStar = -1;
		for (Map.Entry<Integer, Integer> entry : cumulativeDemandSurplus.entrySet()) {
		    if (maxCumulativeDemandSurplus < entry.getValue()) {
		    	maxCumulativeDemandSurplus = entry.getValue();
		    	currentGStar = entry.getKey();
		    }
		}
		
		return currentGStar;
	}

	private static Map<Integer, Integer> getCumulativeDemandSurplus(Bay bay) {
		Set<Integer> groups = getGroupsFromBay(bay);
		Map<Integer, Integer> cumulativeDemandSurplus = new HashMap<>();
		
		for (int groupIndex : groups) {
			int demandSurplusForGroup = getDemandSurplusForGroup(bay, groupIndex);
			cumulativeDemandSurplus.put(groupIndex, demandSurplusForGroup);
		}
		
		return cumulativeDemandSurplus;
	}

	private static int getDemandSurplusForGroup(Bay bay, int groupIndex) {
		int cumulativeDemand = getCumulativeDemandForGroup(bay, groupIndex);
		int cumulativePotentialSupply = getCumulativePotentialSupplyForGroup(bay, groupIndex);
		
		return cumulativeDemand - cumulativePotentialSupply;
	}

	private static int getCumulativePotentialSupplyForGroup(Bay bay, int groupIndex) {
		int cumulativePotentialSupply = 0;
		Set<Integer> groups = getGroupsFromBay(bay);
		
		for (int group : groups) {
			if (group < groupIndex) {
				continue;
			}
			
			int potentialSupplyOfGroup = getPotentialSupplyOfGroup(bay, group);
			cumulativePotentialSupply += potentialSupplyOfGroup;
		}
		
		return cumulativePotentialSupply;
	}

	private static int getPotentialSupplyOfGroup(Bay bay, int group) {
		int potentialSupplyOfGroup = 0;

		int k = bay.getMaxRowIndex() + 1;
		for (int stackIndex = 0; stackIndex < k; stackIndex++) {
			int[] highestWellPlacedItemForStack = getHighestWellPlacedItem(bay, stackIndex);
			int highestWellPlacedItemGroup = highestWellPlacedItemForStack[0];
			int highestWellPlacedTier = highestWellPlacedItemForStack[1];
			if (highestWellPlacedItemGroup == group) {
				potentialSupplyOfGroup += bay.getMaxTierIndex() - highestWellPlacedTier;
			}
		}
		
		return potentialSupplyOfGroup;
	}

	private static int[] getHighestWellPlacedItem(Bay bay, int stackIndex) {
		int l = bay.getMaxColumnIndex(stackIndex);
		for (int tierIndex = 0; tierIndex < l; tierIndex++) {
			if (bay.getValueByCoordinate(stackIndex, tierIndex) < bay.getValueByCoordinate(stackIndex, tierIndex + 1)) {
				return new int[]{bay.getValueByCoordinate(stackIndex, tierIndex), tierIndex};
			}
		}
		
		try {
			return new int[]{bay.getValueByCoordinate(stackIndex, l), l};
		} catch (RuntimeException re) {
			return new int[]{-1, -1};
		}
	}

	private static int getCumulativeDemandForGroup(Bay bay, int groupIndex) {
		int cumulativeDemandForGroup = 0;
		Set<Integer> groups = getGroupsFromBay(bay);
		
		for (int group : groups) {
			if (group < groupIndex) {
				continue;
			}
			
			int demandOfGroup = getDemandOfGroup(bay, group);
			cumulativeDemandForGroup += demandOfGroup;
		}
		
		return cumulativeDemandForGroup;
	}

	private static int getDemandOfGroup(Bay bay, int group) {
		int demandOfGroup = 0;

		int k = bay.getMaxRowIndex() + 1;
		for (int stackIndex = 0; stackIndex < k; stackIndex++) {
			int l = bay.getMaxColumnIndex(stackIndex);
			boolean isDemandForStackCalculated = false;
			for (int tierIndex = 0; tierIndex < l; tierIndex++) {
				if (bay.getValueByCoordinate(stackIndex, tierIndex) < bay.getValueByCoordinate(stackIndex, tierIndex + 1)) {
					for (int i = tierIndex + 1; i <= l; i++) {
						int currentGroup = bay.getValueByCoordinate(stackIndex, i);
						if (currentGroup == group) {
							demandOfGroup++;
						}
					}
					isDemandForStackCalculated = true;
				}
				if (isDemandForStackCalculated) {
					break;
				}
			}
		}
		
		return demandOfGroup;
	}

	private static Set<Integer> getGroupsFromBay(Bay bay) {
		Set<Integer> groups = new TreeSet<>();

		int k = bay.getMaxRowIndex() + 1;
		for (int stackIndex = 0; stackIndex < k; stackIndex++) {
			int l = bay.getMaxColumnIndex(stackIndex);
			for (int tierIndex = 0; tierIndex < l; tierIndex++) {
				int currentBlock = bay.getValueByCoordinate(stackIndex, tierIndex);
				if (!groups.contains(currentBlock)) {
					groups.add(currentBlock);
				}
			}
		}
		
		return groups;
	}

	private static int getLowerBoundOfBX(Bay bay) {
		int numberOfBadPlacedItems = bay.getMisOverlayIndex();
		int minMIForStack = getMinMIForStack(bay);
		return numberOfBadPlacedItems + minMIForStack;
	}

	private static int getMinMIForStack(Bay bay) {
		int minMIForStack = Integer.MAX_VALUE;
		int k = bay.getMaxRowIndex() + 1;
		for (int m = 0; m < k; m++) {
			int currentMI = bay.getMisOverlayIndexForStack(m);
			if (currentMI < minMIForStack) {
				minMIForStack = currentMI;
			}
		}
		
		return minMIForStack;
	}
}
