
public class Coordinate {
	private int row;
	private int column;
	
	public Coordinate(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getRowIndex() {
		return row;
	}
	
	public int getColumnIndex() {
		return column;
	}
	
	@Override
    public boolean equals(Object o) {
        if (o == this) {
        	return true;
        }
        if (!(o instanceof Coordinate)) {
            return false;
        }

        Coordinate c = (Coordinate) o;
        return c.row == row && c.column == column;
	}

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + row;
        result = 31 * result + column;
        return result;
    }
    
    public Coordinate getCoordinate() {
    	return this;	
    }
}
