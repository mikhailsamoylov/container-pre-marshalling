
public interface IParser {
	public Bay getBay();
	public String getLabel();
	public int getWidth();
	public int getHeight();
	public int getContainersCount();
}
