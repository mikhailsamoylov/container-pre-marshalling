import java.util.HashMap;
import java.util.Map;

public class MutationRuleWithStep implements IMutationRule {
	
	private double step;
	
	public MutationRuleWithStep(Double step) {
		this.step = step;
	}
	
	@Override
	public Map<MutationType, Double> getMutationProbabilities(int iteration) {
		Map<MutationType, Double> probs = new HashMap<>();
		probs.put(MutationType.GROWTH, BayGenetics.growthMutationProbability + step * iteration);
		probs.put(MutationType.REPLACE, BayGenetics.replaceMutationProbability + step * iteration);
		probs.put(MutationType.SHRINK, BayGenetics.shrinkMutationProbability + step * iteration);
		probs.put(MutationType.SWAP, BayGenetics.swapMutationProbability + step * iteration);
		
		return probs;
	}

}
