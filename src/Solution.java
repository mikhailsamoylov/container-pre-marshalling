import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Solution {
	
	private String instanceName;
	private Chromosome chromosome;
	private long timeMillis;
	private int popSize;
	private double crossoverProb;
	private double mutationProb;
	
	public Solution(String instanceName, Chromosome chromosome, long timeMillis, int popSize, double crossoverProb, double mutationProb) {
		this.instanceName = instanceName;
		this.chromosome = chromosome;
		this.timeMillis = timeMillis;
		this.popSize = popSize;
		this.crossoverProb = crossoverProb;
		this.mutationProb = mutationProb;
	}
	
	public void save() {
		try (FileWriter fw = new FileWriter(instanceName, true);
				BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
		{
			if (chromosome != null) {
				out.println(popSize + ";" + crossoverProb + ";" + mutationProb + ";" + (timeMillis / 1000) + ";" + chromosome.size());
			} else {
				out.println(popSize + ";" + crossoverProb + ";" + mutationProb + ";Inf;Inf");
			}
		} catch (IOException e) {
			System.err.println("Error while trynna save the solution");
		}
	}
}
