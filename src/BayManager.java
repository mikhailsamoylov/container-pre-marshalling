
public class BayManager {
	public static void moveContainer(Bay bay, int from, int to) {
		bay.setCoordinate(to, bay.getMaxColumnIndex(to) + 1, bay.getValueByCoordinate(from, bay.getMaxColumnIndex(from)));
		bay.deleteCoordinate(new Coordinate(from, bay.getMaxColumnIndex(from)));
	}
}
