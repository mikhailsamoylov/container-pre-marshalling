
public class Gene {
	private int from;
	private int to;
	
	public Gene (int from, int to) {
		this.from = from;
		this.to = to;
	}
	
	public int getFrom() {
		return from;
	}
	
	public void setFrom(int from) {
		this.from = from;
	}
	
	public int getTo() {
		return to;
	}
	
	public void setTo(int to) {
		this.to = to;
	}

	public boolean equals(Object other) {
		if (other instanceof Gene) {
			return from == ((Gene)other).from && to == ((Gene)other).to;
		} else {
			return false;
		}
	}
}
