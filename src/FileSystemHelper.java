

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileSystemHelper {
	
	public static List<String> listFilesForFolder(final File folder) {
		List<String> filenames = new ArrayList<String>();
	    for (final File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            filenames.add(fileEntry.getName());
	        }
	    }
	    return filenames;
	}
	
	public static Map<String, List<String>> listFilesForFolder2(final File folder) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            map.put(fileEntry.getName(), listFilesForFolder(fileEntry));
	        }
	    }
	    return map;
	}
	
	public static void createFolder(final File folder) {
		if (!folder.exists()) {
		    try {
		    	folder.mkdir();
		    } catch (SecurityException e){
		        e.getMessage();
		    }
		}
	}
}
