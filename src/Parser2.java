import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser2 implements IParser {
	
	private String label;
	private int width;
	private int height;
	private int containers;
	private Bay bay;
	
	public Parser2(String filename) {
		bay = new Bay();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String labelRegex = "Label : (.+)";
			String widthRegex = "Width : (\\d+)";
			String heightRegex = "Height : (\\d+)";
			String containerCountRegex = "Containers : (\\d+)";
			String stacksRegex = "Stack (\\d+) : (.+)";
			String containersRegex = "(\\d+)";
			
			label = extractValueFromStringByRegex(br.readLine(), labelRegex);
			width = Integer.parseInt(extractValueFromStringByRegex(br.readLine(), widthRegex));
			height = Integer.parseInt(extractValueFromStringByRegex(br.readLine(), heightRegex));
			containers = Integer.parseInt(extractValueFromStringByRegex(br.readLine(), containerCountRegex));
			
			String temp = br.readLine();
			Matcher matcher;
			Pattern stacksPattern = Pattern.compile(stacksRegex);
			Pattern containersPattern = Pattern.compile(containersRegex);
			while (temp != null) {
				matcher = stacksPattern.matcher(temp);
				int columnPointer = 0;
				if (matcher.find()) {
					int rowPointer = Integer.parseInt(matcher.group(1)) - 1;
					temp = matcher.group(2);
					matcher = containersPattern.matcher(temp);
					while (matcher.find()) {
						bay.setCoordinate(rowPointer, columnPointer, Integer.parseInt(matcher.group(1)));
						columnPointer++;
					}
				}
				
				temp = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private String extractValueFromStringByRegex(String string, String regex) {
		Pattern pattern = Pattern.compile(regex);
		String tempString = string;
		Matcher matcher = pattern.matcher(tempString);
		if (matcher.find()) {
			return matcher.group(1);
		} else {
			return null;
		}
	}

	@Override
	public Bay getBay() {
		return bay;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getContainersCount() {
		return containers;
	}
}
