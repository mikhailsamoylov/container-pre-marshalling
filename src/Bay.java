import java.util.HashMap;

public class Bay extends Matrix<Integer> {
	private int maxTierIndex;
	private int misOverlayIndex;
	private boolean isMisOverlayIndexCalculated = false;
	
	@SuppressWarnings("unchecked")
	public Bay clone() {
        Bay clonedBay = new Bay();
        
        clonedBay.matrix = (HashMap<Coordinate, Integer>) this.matrix.clone();
        clonedBay.maxColumns = (HashMap<Integer, Integer>) this.maxColumns.clone();
        clonedBay.minColumns = (HashMap<Integer, Integer>) this.minColumns.clone();
        clonedBay.maxRow = this.maxRow;
        clonedBay.minRow = this.minRow;
        clonedBay.maxTierIndex = maxTierIndex;
        
        return clonedBay;
    }
	
	public Bay () {
		this(Integer.MAX_VALUE);
	}
	
	public Bay (int maxTierIndex) {
		this.maxTierIndex = maxTierIndex;
	}
	
	public int getMaxTierIndex() {
		return maxTierIndex;
	}
	
	public void setMaxTierIndex(int maxTierIndex) {
		this.maxTierIndex = maxTierIndex;
	}
	
	public int getMisOverlayIndex() {
		if (!isMisOverlayIndexCalculated) {
			calculateMisOverlayIndex();
		}
		
		return misOverlayIndex;
	}
	
	public void setMisOverlayIndex(int misOverlayIndex) {
		this.misOverlayIndex = misOverlayIndex;
	}
	
	private void calculateMisOverlayIndex() {
		// stacks count
		int k = this.getMaxRowIndex() + 1;
		int misOverlayIndex = 0;
		for (int m = 0; m < k; m++) {
			misOverlayIndex += getMisOverlayIndexForStack(m);
		}
		
		this.misOverlayIndex = misOverlayIndex;
		isMisOverlayIndexCalculated = true;
	}
	
	public int getMisOverlayIndexForStack(int stackIndex) {
		// max tier index
		int l = this.getMaxColumnIndex(stackIndex);
		int i = 0;
		int j = l;
		
		int misOverlayIndexForStack = 0;
		while (i < l) {
			if (i >= j) {
				i++;
				j = l;
			}
			
			if (getValueByCoordinate(stackIndex, i) < getValueByCoordinate(stackIndex, j)) {
				misOverlayIndexForStack += l - i;
				for (int n = i + 1; n < j; n++) {
					if (getValueByCoordinate(stackIndex, n - 1) >= getValueByCoordinate(stackIndex, n)) {
						misOverlayIndexForStack--;
					} else {
						break;
					}
				}
				break;
			} else {
				j--;
			}
		}
		
		return misOverlayIndexForStack;
	}
	
	public void setCoordinate(Coordinate c, Integer value) {
		super.setCoordinate(c, value);
		isMisOverlayIndexCalculated = false;
	}
	
	public void deleteCoordinate(Coordinate coordinate) {
		super.deleteCoordinate(coordinate);
		isMisOverlayIndexCalculated = false;
	}
}
