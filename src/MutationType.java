
public enum MutationType {
	GROWTH, SHRINK, SWAP, REPLACE
}
