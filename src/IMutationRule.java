import java.util.Map;

public interface IMutationRule {
	public Map<MutationType, Double> getMutationProbabilities(int iteration);
}
