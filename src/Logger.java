
public class Logger {
	private static volatile boolean isEnabled = false;
	
	public static void setEnabled(boolean flag) {
		isEnabled = flag;
	}
	
	public static void logging(String message) {
		logging(message, true);
	}
	
	public static void logging(String message, boolean isNewLine) {
		if (isEnabled) {
			if (isNewLine) {
				System.out.println(message);
			} else {
				System.out.print(message);
			}
		}
	}
	
	public static void logging(int message, boolean isNewLine) {
		logging(Integer.toString(message), isNewLine);
	}
	
	public static void logging(int message) {
		logging(Integer.toString(message), true);
	}
	
	public static void logging(long message, boolean isNewLine) {
		logging(Long.toString(message), isNewLine);
	}
	
	public static void logging(long message) {
		logging(Long.toString(message), true);
	}
}
