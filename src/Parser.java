import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser implements IParser {
	
	private Bay bay;
	
	public Parser(String filename) {
		bay = new Bay();
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String regexp = "([\\d]+)";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher;
			
			br.readLine();
			String temp = br.readLine();
			int rowPointer = 0, columnPointer = -1;
			
			while (temp != null) {
				matcher = pattern.matcher(temp);
				while (matcher.find()) {
					if (columnPointer >= 0) {
						bay.setCoordinate(rowPointer, columnPointer, Integer.parseInt(matcher.group(1)));
					}
					columnPointer++;
				}
				rowPointer++;
				columnPointer = -1;
				temp = br.readLine();
			}
		} catch (IOException e) {
			System.out.println("File reading error");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public Bay getBay() {
		return bay;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getWidth() {
		return bay.getMaxRowIndex() + 1;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getContainersCount() {
		return bay.getElementNumbers();
	}
}
